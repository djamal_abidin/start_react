import { useLocation } from "react-router-dom";
import TopBar from "../components/TopBar";
import { useState } from "react";
import currencyDollar from '../assets/currencyDollar.svg';

const ListPage = () => {
  const location = useLocation();
  const state = location.state;
  const title = state.term.split(" ").map((x) => x.substring(0, 1).toUpperCase() + x.substring(1).toLowerCase()).join(" ");
  const results = state.results;
  const [list, setList] = useState(results.slice(0, 4));
  const [isLoadMore, setLoadMore] = useState(results.length > list.length);

  const onLoad = () => {
    if (list.length + 4 >= results.length) setLoadMore(false);
    setList(results.slice(0, list.length + 4));
  }
  return ( 
    <div id='list-page' className='container-block'>
	    <TopBar />
      <div className="title-search">
        <span className="f14">Search result for : </span>
        <span className="f18 color-burple">{title}</span>
      </div>
      <div>
        {list.map((l, key) => (
          <div key={key} className='container-list'>
            <img src={l.artworkUrl100} alt="" className='image-list'/>
            <div style={{ marginLeft: 12, width: "calc(100% - 115px)" }}>
              <div style={{ width: "100%", height: 78 }}>
                <p className='f10 m5'>{l.artistName}</p>
                <p className='f14 m5'>{l.collectionCensoredName}</p>
              </div>
              <div style={{ width: "100%", display: "flex", justifyContent: "space-between" }}>
                <div className='f10 label'>{l.primaryGenreName}</div>
                {l.collectionPrice && 
                  <div className='f12 color-gold flex-center'>
                    <img src={currencyDollar} alt="" style={{ marginRight: 6 }}/>{l.collectionPrice}
                  </div>
                }
              </div>
            </div>
          </div>
        ))}
      </div>
      <div style={{ padding: "20px 95px 28px", backgroundColor: "#f8fafc" }}>
        {isLoadMore && 
          <div className="center">
            <button className='button-load' onClick={() => onLoad()}>Load More</button>
          </div>
        }
      </div>
	  </div>
  );
}
 
export default ListPage;