import logoMusic from '../assets/logoMusic.svg';
import InputSearch from '../components/InputSearch';

const Dasboard = () => {
  const openNav = async () => {
	document.getElementById('overlay').style.width = "100%";
  }
  return ( 
    <div id='dashboard' className='dashboard'>
	  <InputSearch />
	  <div className='image-dashboard'>
 	    <img id='img-dashboard' className='center-image' src={logoMusic} alt='' />
	  </div>
	  <div className='relative center'>
		<div style={{ marginBottom: 15 }}>
			<button id='button-dashboard-1' className='button bg-white f12 no-border color-grey'>Artist / Album / Title</button>
		</div>
		<div>
			<button id='button-dashboard-2' className='button bg-burple f14 no-border color-white' onClick={() => openNav()}>Search</button>
		</div>
	  </div>
	</div>
  );
}
 
export default Dasboard;