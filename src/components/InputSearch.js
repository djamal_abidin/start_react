import { useState } from 'react';
import { useNavigate  } from 'react-router-dom';
import searchBlack from '../assets/searchBlack.svg';

const InputSearch = () => {
  const history = useNavigate();
  const [term, setTerm] = useState("");
  const getList = () => {
    fetch(`https://itunes.apple.com/search?term=${term.split(' ').join('+')}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      },
    }).then((res) => {
      return res.json();
    }).then((response) => {
      setTerm("");
      response.term = term;
      console.log(response);
      history("/list", { state: response });
    });
  }

  const closeNav = () => {
    document.getElementById("overlay").style.width = "0%";
  }

  return ( 
    <div id='overlay' className='container overlay' style={{ }}>
      <span className='closebtn' onClick={() => closeNav()}>&times;</span>
	    <div className='container-center'>
        <input id='text' type='text' autoComplete='off' className='input' value={term} onChange={(e) => setTerm(e.target.value)}/>
        <button className='mini-button' onClick={() => getList()}>
          <img id='search' src={searchBlack} alt='' />
        </button>
      </div>
	  </div>
  );
}
 
export default InputSearch;