import { useState } from "react";
import { useNavigate } from "react-router-dom";
import searchBlack from '../assets/searchBlack.svg';

const Search = () => {
  const history = useNavigate();
  const [openInput, setOpenInput] = useState(false);
  const [term, setTerm] = useState("");
  const closeNav = () => {
    setOpenInput(false);
    document.getElementById("overlay-search").style.width = "0%";
  }
  const getList = () => {
    fetch(`https://itunes.apple.com/search?term=${term.split(' ').join('+')}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      },
    }).then((res) => {
      return res.json();
    }).then((response) => {
      setTerm("");
      response.term = term;
      console.log(response);
      closeNav();
      history("/list", { state: response });
    });
  }
  return ( 
    <div id='overlay-search' className='container overlay' style={{ }}>
      <span className='closebtn' onClick={() => closeNav()}>&times;</span>
      <div className='container-center-block' style={{ display: openInput ? "none" : undefined }}>
        <div style={{ marginBottom: 30 }}>
          <p className='f20 color-white center'>Search</p>
        </div>
        <div style={{ marginBottom: 15 }}>
          <button id='button-dashboard-1' className='button bg-white f12 no-border color-grey'>Artist / Album / Title</button>
        </div>
        <div>
          <button id='button-dashboard-2' className='button bg-burple f14 no-border color-white' onClick={() => setOpenInput(true)}>Search</button>
        </div>
      </div>

      <div className='container-center' style={{ display: openInput ? undefined : "none" }}>
        <input id='text' type='text' className='input' autoComplete="off" value={term} onChange={(e) => setTerm(e.target.value)}/>
        <button className='mini-button' onClick={() => getList()}>
          <img id='search' src={searchBlack} alt='' />
        </button>
      </div>
	</div>
  );
}
 
export default Search;