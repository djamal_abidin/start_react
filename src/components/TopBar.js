import menu from '../assets/menu.svg';
import ngmusic from '../assets/ngmusic.svg';
import search from '../assets/search.svg';
import InputSearch from './InputSearch';
import Search from './Search';

const TopBar = () => {
  const openNav = () => {
    document.getElementById("overlay-search").style.width = "100%";
  }
  return ( 
    <div id='top-bar' className='top-bar'>
	  <div className='between'>
	  	<img id='menu' src={menu} alt='' />
	  	<img id='title-top-bar' src={ngmusic} alt='' />
	  	<img id='search' src={search} alt='' onClick={() => openNav()}/>
	  </div>
	  <Search />
	  <InputSearch />
	</div>
  );
}
 
export default TopBar;